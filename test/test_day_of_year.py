from day_of_year import day_of_year, InvalidArgument
import pytest

@pytest.mark.parametrize("day, month, year, expected", [(10, 4, 2000, 101), (1, 11, 1973, 305)])

def test_day_of_year(day, month, year, expected):
    assert day_of_year(day, month, year) == expected

def test_A():
    with pytest.raises(InvalidArgument):
        assert day_of_year(), "Function called with invalid arguments"

def test_B():
    with pytest.raises(InvalidArgument):
        assert day_of_year(30, 2, 2003), "Function called with invalid arguments"

def test_C():
    with pytest.raises(InvalidArgument):
        assert day_of_year(32, 4, 1950), "Function called with invalid arguments"

def test_D():
    with pytest.raises(InvalidArgument):
        assert day_of_year(0, 1, 1987), "Function called with invalid arguments"

def test_E():
    with pytest.raises(InvalidArgument):
        assert day_of_year(17, -12, 2001), "Function called with invalid arguments"

def test_F():
    with pytest.raises(InvalidArgument):
        assert day_of_year(4, 6, -1662), "Function called with invalid arguments"

def test_G():
    with pytest.raises(InvalidArgument):
        assert day_of_year(12, 4), "Function called with invalid arguments"

def test_H():
    with pytest.raises(InvalidArgument):
        assert day_of_year(13, 2.3, 2028), "Function called with invalid arguments"

def test_I():
    with pytest.raises(InvalidArgument):
        assert day_of_year(31, 2, 2004), "Function called with invalid arguments"
