from dos_menores import dos_menores

def test_A():
    assert dos_menores() == None

def test_A2():
    assert dos_menores([]) == None

def test_B():
    assert dos_menores([2,6,8,1]) ==  (1,2)

def test_C():
    assert dos_menores([5]) == (5)

def test_D():
    assert dos_menores(1) == None

def test_E():
    assert dos_menores([8,2]) == (2,8)

def test_F():
    assert dos_menores([3,4]) == (3,4)

def test_G():
    assert dos_menores([6,1,8,2]) ==  (1,2)

