from max_dist import max_dist, InvalidArgument
import pytest

@pytest.mark.parametrize("test,expected", [([2,7], 5), ([9,3], 6), ([1,6,12], 6), ([11, 7, 2],5), ([7,-3,2],10),
                                                 ([3, 56, 8], 53), ([7, 45, 2], 43), ([4,-5,23], 28)])

def test_max_dist(test, expected):
    assert max_dist(test) == expected

def test_A():
    with pytest.raises(InvalidArgument):
        assert max_dist("b"), "Function called with invalid arguments"

def test_B():
    with pytest.raises(InvalidArgument):
        assert max_dist([1]), "Function called with invalid arguments"

def test_C():
    with pytest.raises(TypeError):
        assert max_dist([1, 8.5])

def test_D():
    with pytest.raises(TypeError):
        assert max_dist([1, 3, 7, 4.2, 54])