class InvalidArgument(Exception):
    "Function called with invalid arguments"
    pass

def max_dist(a=None):
    if type(a)!=list or len(a) < 2:
        raise InvalidArgument()

    if type(a[0])!=int or type(a[1])!=int:
        raise TypeError

    val1=a[0]
    val2=a[1]
    max_distance = abs(val1-val2)

    for i in range(2, len(a)):
        if type(a[i])!=int:
            raise TypeError
        else:
            val1=a[i]
            aux_distance=abs(val1-val2)
            if aux_distance>max_distance:
                max_distance=aux_distance
            val2=val1
    return max_distance