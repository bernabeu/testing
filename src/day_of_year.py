class InvalidArgument(Exception):
    "Function called with invalid arguments"
    pass

def bisiesto(year):
    return year % 4 == 0 and (year % 100 != 0 or year % 400 == 0)

def day_of_year(day=None, month=None, year=None):

    if day is None or month is None or year is None: #1
        raise InvalidArgument

    if type(day)!= int or type(month)!= int or type(year)!= int: #6
        raise InvalidArgument

    if month < 1 or month > 12 or year < 1: #5
        raise InvalidArgument

    if month in [1, 3, 5, 7, 8, 10, 12] and (day < 1 or day > 31): #4
        raise InvalidArgument

    if month in [4, 6, 9, 11] and (day < 1 or day > 30): #3
        raise InvalidArgument

    if bisiesto(year) and month == 2 and (day < 1 or day > 29): #7
        raise InvalidArgument

    if not bisiesto(year) and month == 2 and (day < 1 or day > 28): #2
        raise InvalidArgument

    if bisiesto(year): #8
        days = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    else:
        days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    i = 0
    n_days = 0
    while i < (month-1):
        n_days += days[i]
        i += 1
    n_days += day

    return n_days


